// Pilotage du robot autour d'un rectangle
// vitesse approximée du robot : 19cm/s -> 0.19m/s
// vitesse neutre : 1500
// vitesse max roue de gauche : 1700
// vitesse max roue de droite : 1300
// delay pour 90° en full speed : 450ms

#include <Servo.h>                         
 
Servo servoLeft;                            
Servo servoRight;
 
void setup()                               
{ 
  Serial.begin(9600);      // open the serial port at 9600 bps:
  servoLeft.attach(13);                      // Attach left signal to P13 
  servoRight.attach(12);                     // Attach right signal to P12

  forward(1.0);
  turn(-90.0);
  forward(1.0);
  turn(-90.0);
  forward(1.0);
  turn(-90.0);
  forward(1.0);
  
  servoLeft.detach();                        // Stop sending servo signals
  servoRight.detach(); 
}  
 
void loop()                                
{     

}

// avancer devant en mètre
void forward(float m)
{
  float time = m/0.19;
  servoLeft.writeMicroseconds(1700);         // left wheel full speed
  servoRight.writeMicroseconds(1300);        
  delay((int)time*1000); //ms                             
}

// tourner 
void turn(float degre)
{
  float time = 0;
  float refTime = 450; // to turn 90 degres
  if(degre > 0){
     time = degre*refTime/90;
     Serial.print(time);
     servoLeft.writeMicroseconds(1300);         // left wheel full speed
     servoRight.writeMicroseconds(1300);     
     delay((int)time);
  }
  else {
    time = -degre*refTime/90;
    Serial.print(time);
    servoLeft.writeMicroseconds(1700);         // left wheel full speed
    servoRight.writeMicroseconds(1700); 
    delay((int)time);
  } 
  
}


