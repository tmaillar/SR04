# Utiliser contiki
## Pré-requis
Le materiel dont nous disposons test identifié comme *openmote-cc2538*. Elle possède un processeur ARM, par conséquent il faut cross-compiler du code C faisant appel au Framework Contiki.

### Installation des dépendances
```bash
sudo apt update
sudo apt install putty default-jre default-jdk git-core python-serial libncurses5-dev libncursesw5-dev make gcc libncurses5-dev libncursesw5-dev gcc-msp430 gcc-arm-none-eabi build-essential
```

### Compilation d'outils
```bash
cd tools/sky/
gcc serialdump.c -o serialdump-linux
```
## Flasher une OpenMote
Dans cet exemple on veut mettre le code du dossier `hello-world` sur une carte. Il faut tout d'abord brancher la carte et :

```bash
cd hello-world
make TARGET=openmote-cc2538 savetarget
sudo make BOARD_REVISION=REV_A1 hello-world.upload
```

## Debogage
Pour intéragir avec une openmote, on utilise un port série mis en place lorsqu'elle est branchée en USB au boot. Pour cela, la meilleure option trouvée jusque là est d'ouvrir une session port série avec putty:

```bash
sudo putty
```
* Choisir un baud rate de 115200
* Le chemin vers le port série est de la forme `/dev/ttyUSB0`
* Démarrer la session, puis fermer (car illisible).

La session étant démarrée, elle est maintenant lisible avec
```bash
cat /dev/ttyUSB0
```

## Utiliser le logiciel de simulation Cooja
```bash
cd tools/cooja/
ant run
```
