#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#define PI 3.1415926535897932384626433832795
#define FreqInMHz 2400
#define ReferencDistance 1

struct coordinate {
    double x;
    double y;
    double r;
};

struct transformation {
    double x;
    double y;
    double alpha;
};


struct beacon {
    const char *name;
    double RSSref;
    double RSS;
    double x;
    double y;
};

#define SQR(x) (x * x)


/* Perform the trilateration for the given 3 terms */
struct coordinate trilaterate(struct coordinate * terms) {
    
    struct coordinate result;
    double i = terms[2].x - terms[0].x;
    double j = terms[2].y - terms[0].y;
    double d = terms[1].x;
    
    result.x = (SQR(terms[0].r) - SQR(terms[1].r) + SQR(d)) / (2 * d);
    
    result.y = ( (SQR(terms[0].r) - SQR(terms[2].r) + SQR(i) + SQR(j) ) / (2 * j) ) - ((i / j) * result.x);
    
    return result;
}


/*
 Modify our coordinate system and apply the new system to the given terms.
 The modifications leave the following state to be true:
 terms[0].x == 0 && terms[0].y == 0 && terms[0].z == 0
 terms[1].y == 0
 
 Returns information about the alterations used to restore the coordinate system later.
 */
struct transformation coordinateTransformation(struct coordinate * terms) {
    int i;
    struct transformation map;
    map.x = -terms[0].x;
    map.y = -terms[0].y;
    
    // Center coordinate system
    for(i = 0; i < 3; i++) {
        terms[i].x += map.x;
        terms[i].y += map.y;
    }
    
    // Rotate coordinate system such that terms[1].y == 0
    double a; // width
    double b; // height
    double c; // hypotenuse
    
    a = (terms[1].x);
    b = (terms[1].y);
    c = sqrt(SQR(a) + SQR(b));
    
    map.alpha = acos(((SQR(a) + SQR(c)) - SQR(b)) / (2.0 * a * c));
    
    if ((a > 0 && b > 0) || (a < 0 && b > 0)) {
        map.alpha *= -1;
    }
    
    for(i = 0; i < 3; i++) {
        // Rotate x and y
        double x, y;
        x = terms[i].x * cos(map.alpha) - terms[i].y * sin(map.alpha);
        y = terms[i].x * sin(map.alpha) + terms[i].y * cos(map.alpha);
        terms[i].x = x;
        terms[i].y = y;
    }
    return map;
}


/* Reverse and restore the coordinate system for the given coordinate */
struct coordinate inverseCoordinateTransformation(struct coordinate coord, struct transformation map) {
    double x = coord.x * cos(-map.alpha) - coord.y * sin(-map.alpha);
    double y = coord.x * sin(-map.alpha) + coord.y * cos(-map.alpha);
    
    coord.x = x;
    coord.y = y;
    coord.x -= map.x;
    coord.y -= map.y;
    return coord;
}


double distanceFromRSS(double rss, double reference) {
    // TODO: ideal: n^2. real life: n^3 - n^5
    return pow(10.0, (rss-reference)/20) * ReferencDistance;
}


double distanceFromOnlyRSS(double rss) {
    double exp = (27.55 - (20 * log10(FreqInMHz)) + fabs(rss)) / 20.0;
    return pow(10.0, exp);
}


/* init beacon with a name, its position and a reference signal strength */
void initBeacon(struct beacon *beacon, const char *name, float refRSS, double x, double y) {
    beacon->name = name;
    beacon->x = x;
    beacon->y = y;
    beacon->RSSref = refRSS;
}


void initBeaconForTrilateration(struct coordinate *b, struct beacon *beacon) {
    b->x = beacon->x;
    b->y = beacon->y;
    b->r = distanceFromRSS(beacon->RSS, beacon->RSSref);
}


void updateBeacon(struct beacon beacons[3], const char *name, float RSS) {
    for (int i=0; i<3; i++) {
        if (strcmp(beacons[i].name, name) == 0) {
            beacons[i].RSS = RSS;
            break;
        }
    }
}

                                       
struct coordinate calculatePosition(struct coordinate beacons[3]) {
    struct coordinate result;
    struct transformation restore;
    // Alter the coordinate system to center terms[0] and rotate the plane to ensure terms[1].y == 0
    restore = coordinateTransformation(beacons);
    
    // Trilaterate the 3 points and get the intersection
    result = trilaterate(beacons);
    
    // Restore the previous coordinate system
    result = inverseCoordinateTransformation(result, restore);
    return result;
}


// example
int main(int argc, char ** argv) {
    // setup
    struct beacon beacons[3];
    initBeacon(&beacons[0], "aa", 4, 1, 2);
    initBeacon(&beacons[1], "bb", 10, 3, 8);
    initBeacon(&beacons[2], "cc", 40, 5, 4);
    
    // main loop
    updateBeacon(beacons, "aa", 3);
    updateBeacon(beacons, "cc", 8);
    updateBeacon(beacons, "bb", 4);

    
    struct coordinate refs[3];
    initBeaconForTrilateration(&refs[0], &beacons[0]);
    initBeaconForTrilateration(&refs[1], &beacons[1]);
    initBeaconForTrilateration(&refs[2], &beacons[2]);

    struct coordinate result = calculatePosition(refs);
    
    printf("Result = (%.3f,  %.3f)\n", result.x, result.y);
}
