// Pilotage du robot autour d'un rectangle
// vitesse approximée du robot : 19cm/s -> 0.19m/s
// vitesse neutre : 1500
// vitesse max roue de gauche : 1700
// vitesse max roue de droite : 1300
// delay pour 90° en full speed : 450ms

#include <Servo.h>
#include <SoftwareSerial.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

Servo servoLeft;
Servo servoRight;

const int mrect = 1;
const int nrect = 1;

typedef struct coordinates{
    int x;
    int y;
}coordinates;

float rotRobot;
coordinates tabOpenmotes[3];

int state = 0;
float Time;
bool noEvent = true, nextMove = true;
unsigned long mil = 0;

String input[3];
int in[3];
char c;

// m : longueur, n : largeur
void navRect(float m, float n){
  forward(m);
  turn(-90.0);
  forward(n);
  turn(-90.0);
  forward(m);
  turn(-90.0);
  forward(n);
}

float angleRot(coordinates posRobot, int openmoteToReach){
  //Calcul angle du triangle rectangle dont l'hypothénuse est le segment OM - robot
  double x = (double)tabOpenmotes[openmoteToReach].x - (double)posRobot.x;
  double y = (double)tabOpenmotes[openmoteToReach].y - (double)posRobot.y;
  if (x == 0 || y == 0)
  {
    double aOM = atan2((double)tabOpenmotes[openmoteToReach].x, (double)tabOpenmotes[openmoteToReach].y);
    if (aOM != rotRobot)
    {
      return 180;
    }
  }

  double a = atan2(x, y);
  if (x > 0 && y > 0)
    return -rotRobot +(float) a;
  if (x > 0 && y < 0)
    return -rotRobot + 180 + (float) a;
  if (x < 0 && y > 0)
    return -rotRobot + 180 - (float) a;
  if (x < 0 && y < 0)
    return -rotRobot + (float) -a;
}

// avancer devant en mètre
float forward(float m){
  float Time = m/0.19;
  servoLeft.writeMicroseconds(1700);         // left wheel full speed
  servoRight.writeMicroseconds(1300);
  return Time*1000;
}

// tourner
float turn(float degre)
{
  float Time = 0;
  float refTime = 530; // to turn 90 degres
  if(degre > 0){
     Time = degre*refTime/90;
     servoLeft.writeMicroseconds(1300);         // left wheel full speed
     servoRight.writeMicroseconds(1300);
  }
  else {
    Time = -degre*refTime/90;
    servoLeft.writeMicroseconds(1700);         // left wheel full speed
    servoRight.writeMicroseconds(1700);
  }
  rotRobot += degre;
  return Time;
}

void setup()
{
  Time = forward(1.0f);
  tabOpenmotes[0].x = 0;
  tabOpenmotes[0].y = 10;
  tabOpenmotes[1].x = 5;
  tabOpenmotes[1].y = 0;
  tabOpenmotes[2].x = 10;
  tabOpenmotes[2].y = 10;
  rotRobot = 0;
  Serial.begin(115200);
  Serial.println("RESET");
  servoLeft.attach(13);                      // Attach left signal to P13
  servoRight.attach(12);                     // Attach right signal to P12
}


void loop()
{
  //gestion robot rectangle
  if(noEvent) {
    if(nextMove){
      mil = millis();
      nextMove = false;
    }
    if(millis() - mil > Time) {
      if(state == 1) {
        state = 0;
        Time = forward(1.0f);
        nextMove = true;
      }
      else {
        state = 1;
        Time = turn(-90.0f);
        nextMove = true;
      }
    }

    switch(state){
      case 0:
        forward(1.0f);
      break;
      case 1:
        turn(90.0f);
        break;
      default :
        break;
    }
  }

  //non blocking transmission faster than Arduino
  if(Serial.available() > 0){
      for(int i=0;i<3;++i){
        input[i]="";
        while((c=(char) Serial.read())!=';' && c!='\n'){
          input[i].concat(c);
          Serial.println(input[i] + " read " + c);
        }
        in[i]=input[i].toInt();
        Serial.println(in[i]);
      }
      treatment();
   }
}

void treatment(){
  noEvent=false;
  //Serial.print("\nGOT " + x + ";" + y + ";" + id + "\n");
  coordinates robCoor;
  robCoor.x = in[0];
  robCoor.y = in[1];
  float rot = angleRot(robCoor, in[2]);
  float duration = turn(rot);
  char tmp[128];
  sprintf(tmp,"Turn during %d\n", (int) rot);//(duration>0.0)?(int) duration:-1);
  Serial.print(tmp);
  delay(duration);

  float dist = sqrt ( pow((double)robCoor.x + (double)tabOpenmotes[in[2]].x, 2) + pow((double)robCoor.y + (double)tabOpenmotes[in[2]].y, 2) );
  // distance en m
  duration = forward(dist);
  sprintf(tmp,"Go %dm during %lds\n",(int) dist,(long) duration);
  Serial.print(tmp);
  delay(duration);
}
