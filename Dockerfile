FROM debian:latest
RUN sed 's&http://deb.debian.org/debian&http://cdn-fastly.deb.debian.org/debian&' -i /etc/apt/sources.list
RUN apt update && apt install -yq putty default-jre default-jdk git-core python-serial libncurses5-dev libncursesw5-dev make gcc libncurses5-dev libncursesw5-dev gcc-msp430 gcc-arm-none-eabi build-essential

ADD . /root
