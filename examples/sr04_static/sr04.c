#include "sr04.h"

void printAdress(){
  extern linkaddr_t linkaddr_node_addr;
  int i;
  printf("OpenMote linkaddr_t: ");
  for (i = 0; i < LINKADDR_SIZE; i++)
    printf("%x", linkaddr_node_addr.u8[i]);
  printf("\n");
}
