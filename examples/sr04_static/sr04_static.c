#include "contiki.h"
#include "net/rime/rime.h"
#include "net/rime/mesh.h"

#include "dev/button-sensor.h"
#include "dev/sht21.h"
#include "dev/leds.h"

#include "sr04.h"

PROCESS(sr04_static_process, "SR04 static");
AUTOSTART_PROCESSES(&sr04_static_process);

bool anomaly = false;
Report report, anomaly_report;
Message msg;
static struct mesh_conn mesh;
static struct broadcast_conn broadcast;

void readSensors(){
  leds_on(LEDS_ORANGE);
  report.timestamp = clock_seconds();
  report.temperature = sht21.value(SHT21_READ_TEMP);
  report.humidity = sht21.value(SHT21_READ_RHUM);
  float temp = report.temperature / 100.0;
  if(temp > MAX_TEMP && !anomaly){
    anomaly = true;
    anomaly_report = report;
  }
  printf("Temperature: %fC\n", temp);
  printf("Rel. humidity: %u.%u%%\n", report.humidity / 100, report.humidity % 100);
  leds_off(LEDS_ORANGE);
}

static void sent(struct mesh_conn *c){
  printf("packet sent\n");
  leds_toggle(LEDS_GREEN);
  anomaly = false;
  readSensors();
}

static void timedout(struct mesh_conn *c){
  printf("packet timedout\n");
  leds_toggle(LEDS_RED);
}

static void sendMsg(const linkaddr_t *to){
  msg.anomaly = anomaly;
  msg.current = report;
  msg.faulty = anomaly_report;
  packetbuf_copyfrom(&msg, sizeof(msg));
  mesh_send(&mesh, to);
}

static void broadcast_recv(struct broadcast_conn *c, const linkaddr_t *from){
  printf("Data received from %d.%d: %.*s (%d)\n", from->u8[0], from->u8[1], packetbuf_datalen(), (char *)packetbuf_dataptr(), packetbuf_datalen());

  if (from->u8[0]==robot_addr.u8[0] && from->u8[1]==robot_addr.u8[1]) {
    sendMsg(from);
  }
}

static void recv(struct mesh_conn *c, const linkaddr_t *from, uint8_t hops){
  broadcast_recv(NULL,from);
}

const static struct mesh_callbacks callbacks = {recv, sent, timedout};
static const struct broadcast_callbacks broadcast_call = {broadcast_recv};

/* Wait for button click before sending the first message.
PROCESS_WAIT_EVENT_UNTIL(ev == sensors_event && data == &button_sensor);
*/

PROCESS_THREAD(sr04_static_process, ev, data){
  PROCESS_EXITHANDLER(mesh_close(&mesh);broadcast_close(&broadcast);)
  PROCESS_BEGIN();

  printAdress();
  mesh_open(&mesh, 132, &callbacks);
  broadcast_open(&broadcast, 129, &broadcast_call);
  clock_init();
  static struct etimer et;
  static uint16_t sht21_present;

  /* Initialize the SHT21 sensor */
  sht21_present = SENSORS_ACTIVATE(sht21);
  if(sht21_present == SHT21_ERROR) {
    printf("SHT21 sensor is NOT present!\n");
    leds_on(LEDS_RED);
  }
  anomaly_report.timestamp = anomaly_report.temperature = anomaly_report.humidity = 0;
  report.timestamp = report.temperature = report.humidity = 0;

  while(1) {
    etimer_set(&et, CLOCK_SECOND);
    PROCESS_YIELD(); //wait for any event
    if (ev==PROCESS_EVENT_TIMER) {
        if(sht21_present != SHT21_ERROR)
          readSensors();
      } else if (ev == sensors_event)
        sendMsg(&robot_addr);
    }

  PROCESS_END();
}
