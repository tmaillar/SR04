#include "contiki.h"
#include "net/rime/rime.h"
#include "net/rime/mesh.h"

#include "dev/button-sensor.h"
#include "dev/leds.h"

#include "sr04.h"
#include "tri.h"

PROCESS(sr04_robot_process, "SR04 robot");
AUTOSTART_PROCESSES(&sr04_robot_process);

static struct mesh_conn mesh;
static struct broadcast_conn broadcast;
static struct beacon beacons[3];
static unsigned char cstatic_addr[3][2];

void toChar(const linkaddr_t* addr, unsigned char* dest){
  dest[0]=addr->u8[0];
  dest[1]=addr->u8[1];
}

static void sent(struct mesh_conn *c){
  //printf("packet sent\n");
}

static void timedout(struct mesh_conn *c){
  //printf("packet timedout\n");
  leds_toggle(LEDS_RED);
}

static void sendRequest(){
  packetbuf_copyfrom(NULL, 0);
  broadcast_send(&broadcast);
  leds_toggle(LEDS_GREEN);
}

int getIndex(unsigned char* id){
  int array_size = sizeof(static_addr)/sizeof(static_addr[0]);
  int i;
  for (i = 0; i < array_size; i++)
    if (id[0]==static_addr[i].u8[0] && id[1]==static_addr[i].u8[1])
      return i;

  return -1;
}

static void recv(struct mesh_conn *c, const linkaddr_t *from, uint8_t hops){
  leds_toggle(LEDS_ORANGE);
  Message *msg = (Message *) packetbuf_dataptr();
  Record r;
  r.msg = *msg;
  r.from = *from;
  r.intensity = packetbuf_attr(PACKETBUF_ATTR_RSSI);
  unsigned char cfrom[2];
  toChar(&r.from,cfrom);
  updateBeacon(beacons, cfrom, r.intensity);
  // printf("Update %x %x - %d - %d dB - %d\n",cfrom[0], cfrom[1], hops, r.intensity, r.msg.current.temperature);

  if (r.msg.anomaly) {
    struct coordinate refs[3];
    initBeaconForTrilateration(&refs[0], &beacons[0]);
    initBeaconForTrilateration(&refs[1], &beacons[1]);
    initBeaconForTrilateration(&refs[2], &beacons[2]);

    struct coordinate result={0.0,0.0,0.0};
    result = trilaterateLeastSqrt(refs, 3);
    int a=getIndex(cfrom);
    if(a>=0)
      printf("%d;%d;%d\n", (int) result.x, (int) result.y, a);//, r.intensity
  }
}

static void broadcast_recv(struct broadcast_conn *c, const linkaddr_t *from){
  // printf("broadcast message received from %d.%d: '%s'\n",from->u8[0], from->u8[1], (char *)packetbuf_dataptr());
}

const static struct mesh_callbacks callbacks = {recv, sent, timedout};
static const struct broadcast_callbacks broadcast_call = {broadcast_recv};

PROCESS_THREAD(sr04_robot_process, ev, data){
  PROCESS_EXITHANDLER(mesh_close(&mesh);broadcast_close(&broadcast);)
  PROCESS_BEGIN();

  printAdress();
  mesh_open(&mesh, 132, &callbacks);
  broadcast_open(&broadcast, 129, &broadcast_call);
  clock_init();
  static struct etimer et;

  toChar(&static_addr[0],cstatic_addr[0]);
  toChar(&static_addr[1],cstatic_addr[1]);
  toChar(&static_addr[2],cstatic_addr[2]);

  // trialteration init
  /*
  FIXME need to be updated with real positions & reference RSSI
  */
  // beacons, id, RSSI_ref, x, y
  initBeacon(&beacons[0], cstatic_addr[0], -60, 0, 10);
  initBeacon(&beacons[1], cstatic_addr[1], -60, 5, 0);
  initBeacon(&beacons[2], cstatic_addr[2], -60, 10, 10);

  while(1) {
    etimer_set(&et, CLOCK_SECOND);
    PROCESS_YIELD(); //wait for any event
    sendRequest();
  }
  PROCESS_END();
}
