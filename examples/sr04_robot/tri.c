#include "tri.h"

/* Perform the trilateration for the given 3 terms */
struct coordinate trilaterate(struct coordinate *terms) {

    struct coordinate result;
    double i = terms[2].x - terms[0].x;
    double j = terms[2].y - terms[0].y;
    double d = terms[1].x;

    result.x = (SQR(terms[0].r) - SQR(terms[1].r) + SQR(d)) / (2 * d);

    result.y = ( (SQR(terms[0].r) - SQR(terms[2].r) + SQR(i) + SQR(j) ) / (2 * j) ) - ((i / j) * result.x);

    return result;
}


/*
 Modify our coordinate system and apply the new system to the given terms.
 The modifications leave the following state to be true:
 terms[0].x == 0 && terms[0].y == 0 && terms[0].z == 0
 terms[1].y == 0

 Returns information about the alterations used to restore the coordinate system later.
 */
struct transformation coordinateTransformation(struct coordinate *terms) {
    int i;
    struct transformation map;
    map.x = -terms[0].x;
    map.y = -terms[0].y;

    // Center coordinate system
    for(i = 0; i < 3; i++) {
        terms[i].x += map.x;
        terms[i].y += map.y;
    }

    // Rotate coordinate system such that terms[1].y == 0
    double a; // width
    double b; // height
    double c; // hypotenuse

    a = (terms[1].x);
    b = (terms[1].y);
    c = sqrt(SQR(a) + SQR(b));

    map.alpha = acos(((SQR(a) + SQR(c)) - SQR(b)) / (2.0 * a * c));

    if ((a > 0 && b > 0) || (a < 0 && b > 0)) {
        map.alpha *= -1;
    }

    for(i = 0; i < 3; i++) {
        // Rotate x and y
        double x, y;
        x = terms[i].x * cos(map.alpha) - terms[i].y * sin(map.alpha);
        y = terms[i].x * sin(map.alpha) + terms[i].y * cos(map.alpha);
        terms[i].x = x;
        terms[i].y = y;
    }
    return map;
}


/* Reverse and restore the coordinate system for the given coordinate */
struct coordinate inverseCoordinateTransformation(struct coordinate coord, struct transformation map) {
    double x = coord.x * cos(-map.alpha) - coord.y * sin(-map.alpha);
    double y = coord.x * sin(-map.alpha) + coord.y * cos(-map.alpha);

    coord.x = x;
    coord.y = y;
    coord.x -= map.x;
    coord.y -= map.y;
    return coord;
}


double distanceFromRSS(double rss, double reference) {
    // TODO: ideal: n^2. real life: n^3 - n^5
    return pow(10.0, (rss-reference)/20) * ReferencDistance;
}


double distanceFromOnlyRSS(int rss) {
    double ret = pow(2.718,-0.1597*rss-7.042); // in meters
    //printf("%d -> %d\n", rss, (int) (ret*100));
    return ret;
}


/* init beacon with a name, its position and a reference signal strength */
void initBeacon(struct beacon *beacon, const unsigned char *name, int refRSS, double x, double y) {
    beacon->name = name;
    beacon->x = x;
    beacon->y = y;
    beacon->RSSref = refRSS;
}


void initBeaconForTrilateration(struct coordinate *b, struct beacon *beacon) {
    b->x = beacon->x;
    b->y = beacon->y;
    b->r = distanceFromOnlyRSS(beacon->RSS);
    //printf("%d\n",(int) (b->r*100));
}

int adrcmp(const unsigned char* a, const unsigned char* b){
  int i;
  for(i=0;i<2;++i){
    if(a[i]!=b[i])
      return 0;
  }
  return 1;
}

void updateBeacon(struct beacon *beacons, const unsigned char *name, int RSS) {
    int i;
    for (i=0; i<3; i++) {
        if (adrcmp(beacons[i].name, name)) {
            beacons[i].RSS = RSS;
            break;
        }
    }
}


struct coordinate calculatePosition(struct coordinate beacons[3]) {
    struct coordinate result;
    struct transformation restore;
    // Alter the coordinate system to center terms[0] and rotate the plane to ensure terms[1].y == 0
    restore = coordinateTransformation(beacons);

    // Trilaterate the 3 points and get the intersection
    result = trilaterate(beacons);

    // Restore the previous coordinate system
    result = inverseCoordinateTransformation(result, restore);
    return result;
}


struct coordinate leastSqrt(double *x, double *y, int n) {
    double SUMx, SUMy, SUMxy, SUMxx, SUMres, res, slope,
    y_intercept, y_estimate ;
    int i;

//    x = (double *) malloc (n*sizeof(double));
//    y = (double *) malloc (n*sizeof(double));

    SUMx = 0; SUMy = 0; SUMxy = 0; SUMxx = 0;
    for (i=0; i<n; i++) {
        SUMx = SUMx + x[i];
        SUMy = SUMy + y[i];
        SUMxy = SUMxy + x[i]*y[i];
        SUMxx = SUMxx + x[i]*x[i];
    }
    slope = ( SUMx*SUMy - n*SUMxy ) / ( SUMx*SUMx - n*SUMxx );
    y_intercept = ( SUMy - slope*SUMx ) / n;

    //printf ("\n");
    //printf ("The linear equation that best fits the given data:\n");
    //printf ("       y = %6.2lfx + %6.2lf\n", slope, y_intercept);
//    //printf ("--------------------------------------------------\n");
//    //printf ("   Original (x,y)     Estimated y     Residual\n");
//    //printf ("--------------------------------------------------\n");

    SUMres = 0;
    for (i=0; i<n; i++) {
        y_estimate = slope*x[i] + y_intercept;
        res = y[i] - y_estimate;
        SUMres = SUMres + res*res;
//        printf ("   (%6.2lf %6.2lf)      %6.2lf       %6.2lf\n",
//                x[i], y[i], y_estimate, res);
    }
    //printf("--------------------------------------------------\n");
    //printf("Residual sum = %6.2lf\n", SUMres);
    struct coordinate ret;
    ret.x = y_intercept;
    ret.y = slope;
    ret.r = SUMres;
    return ret;
}

struct coordinate trilaterateLeastSqrt(struct coordinate *beacons, int n) {
    int i;
    double a[n],b[n],c[n],u[n-1],v[n-1];
    for (i=1; i<n; i++) {
        a[i-1] = 2 * (beacons[0].x - beacons[i].x);
        b[i-1] = 2 * (beacons[0].y - beacons[i].y);
        c[i-1] = pow(beacons[i].r,2) - pow(beacons[0].r,2) + pow(beacons[0].x,2) + pow(beacons[0].y,2) - pow(beacons[i].x,2) - pow(beacons[i].y,2);
    }
    for (i=0; i<n-1; i++) {
        u[i] = b[i] / a[i];
        v[i] = c[i] / a[i];
    }
    return leastSqrt(u, v, n-1);
}


// // example
// int main(int argc, char ** argv) {
//     // setup
//     struct beacon beacons[3];
//     initBeacon(&beacons[0], "aa", 4, 0, 2);
//     initBeacon(&beacons[1], "bb", 10, 3, 8);
//     initBeacon(&beacons[2], "cc", 40, 5, 4);
//
//     // main loop
//     updateBeacon(beacons, "aa", 3);
//     updateBeacon(beacons, "cc", 8);
//     updateBeacon(beacons, "bb", 4);
//
//
//     struct coordinate refs[3];
//     initBeaconForTrilateration(&refs[0], &beacons[0]);
//     initBeaconForTrilateration(&refs[1], &beacons[1]);
//     initBeaconForTrilateration(&refs[2], &beacons[2]);
//     refs[0].r = 3.5;
//     refs[1].r = 1;
//     refs[2].r = 1;
//     //printf("(%2f, %2f) %.2f\n", refs[0].x, refs[0].y, refs[0].r);
//     //printf("(%2f, %2f) %.2f\n", refs[1].x, refs[1].y, refs[1].r);
//     //printf("(%2f, %2f) %.2f\n", refs[2].x, refs[2].y, refs[2].r);
//
//     struct coordinate result2 = trilaterateLeastSqrt(refs, 3);
//     struct coordinate result = calculatePosition(refs);
//
//     //printf("Result = (%.3f,  %.3f)\n", result.x, result.y);
//     //printf("Result = (%.3f,  %.3f)\n", result2.x, result2.y);
//
// }
