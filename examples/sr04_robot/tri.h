#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#define PI 3.1415926535897932384626433832795
#define FreqInMHz 2400
#define ReferencDistance 1

struct coordinate {
    double x;
    double y;
    double r;
};

struct transformation {
    double x;
    double y;
    double alpha;
};

struct beacon {
    const unsigned char *name;
    double RSSref;
    int RSS;
    double x;
    double y;
};

#define SQR(x) (x * x)

struct coordinate calculatePosition(struct coordinate beacons[3]) ;
void updateBeacon(struct beacon *beacons, const unsigned char *name, int RSS);
void initBeaconForTrilateration(struct coordinate *b, struct beacon *beacon);
void initBeacon(struct beacon *beacon, const unsigned char *name, int refRSS, double x, double y);
struct coordinate trilaterateLeastSqrt(struct coordinate *beacons, int n);
