#include <stdio.h>
#include "net/linkaddr.h"

#define MAX_TEMP 23

typedef enum { false, true } bool;

typedef struct {
	int16_t temperature, humidity;
  unsigned long timestamp;
} Report;

typedef struct {
	bool anomaly;
	Report current, faulty;
} Message;

typedef struct {
	Message msg;
	linkaddr_t from;
	signed char intensity;
} Record;

// Will be raised if temperature > MAX_TEMP
extern bool anomaly;
const static linkaddr_t robot_addr = {.u8={0x54,0x44}};
/*
	FIXME to complete properly with the right addresses
	it's printed at bootime
	Has to be sorted as the arduino array
*/
const static linkaddr_t static_addr[] = {{.u8={0x53,0x24}},{.u8={0x53,0x22}},{.u8={0x53,0x16}}};

void printAdress();
